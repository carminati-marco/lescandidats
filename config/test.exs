use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :lesCandidats, LesCandidatsWeb.Endpoint,
  http: [port: System.get_env("PORT") || 4000],
  server: true

config :lesCandidats, :sql_sandbox, true

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :lesCandidats, LesCandidats.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: System.get_env("POSTGRES_USER") || "postgres",
  password: System.get_env("POSTGRES_PASSWORD") || "postgres",
  database: System.get_env("POSTGRES_DB") || "lesCandidats_graphql_test",
  hostname: System.get_env("POSTGRES_HOST") || "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

# Configures Bamboo
config :lesCandidats, LesCandidats.Mailer, adapter: Bamboo.TestAdapter

config :ex_aws,
  access_key_id: ["fake", :instance_role],
  secret_access_key: ["fake", :instance_role],
  region: "fakes3"

config :ex_aws, :s3,
  scheme: "http://",
  host: "localhost",
  port: 4567

config :arc,
  storage: Arc.Storage.S3,
  asset_host: "http://localhost:4567/lesCandidats-phoenix-graphql",
  bucket: "lesCandidats-phoenix-graphql"
