# LesCandidats Phoenix GraphQL

This application uses the follow stack: **Phoenix**, **React**, and **GraphQL**.
This is a board where some candidats are loaded and you can change their own status.

## Technologies

### Frontend

- [React](https://facebook.github.io/react) - A JavaScript library for building user interfaces. It introduces many great concepts, such as, Virtual DOM, Data flow, etc.
- [Apollo 2](http://dev.apollodata.com) - A flexible, fully-featured GraphQL client for every platform.
- [Material-ui](https://material-ui.com) - React components for faster and easier web development.
- [React-DND](https://github.com/react-dnd/react-dnd) - Drag and Drop for React.

### Backend

- Elixir 1.6
- Phoenix 1.3
- Erlang 20.3
- [Absinthe](https://github.com/absinthe-graphql/absinthe) - The [GraphQL](http://graphql.org) toolkit for Elixir.
- [Graphiql](https://github.com/graphql/graphiql) - Graphiql is an in-browser IDE for exploring GraphQL.
- PostgreSQL for database.

## Prerequisites

- Docker to create the 2 containers the backend
- ReactJS for the frontend

## Getting Started

- Checkout the lesCandidats git tree from Gitlab

          $ git clone https://gitlab.com/carminati-marco/lescandidats.git
          $ cd lesCandidats
          $ make build_all

- _NOTE_ make build_all will create the 2 containers (web + postgresql) and launch the migration + loading samples for backend. For the frontend, it will launch the app using the yarn commands.
  If you don't see any data in frontend, wait some seconds (the needed time to the backend in order to finish its job)
  see 'makefile' if you want to change or debug the created commands (you can use build and exec_web separatly).

## Testing

in the own directories

1. Backend: Run tests with `mix test`
2. Frontend: Run tests with `yarn test`

## To do

1. To intregrate functional tests.
2. To add form and related graphql mutation + subscription in order to add new candidats.
3. Any idea would be appreciated
