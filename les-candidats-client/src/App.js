import React from "react";

import Typography from "@material-ui/core/Typography";
import Header from "./components/Header";
import Board from "./components/Board";

function App() {
  return (
    <div>
      <Header />
      <Typography component="div">
        <Board />
      </Typography>
    </div>
  );
}

export default App;
