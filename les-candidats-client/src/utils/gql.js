import gql from "graphql-tag";

export const GET_CANDIDATS = gql`
  query Candidats {
    candidats {
      id
      imageUrl
      name
      description
      status
      rating
      likes_cnt
      comments_cnt
      emails_cnt
    }
  }
`;

export const UPDATE_STATE_CANDIDAT = gql`
  mutation upvotePost($id: Int!, $status: String!) {
    updateCandidat(id: $id, input: { status: $status }) {
      successful
    }
  }
`;

export const CANDIDATS_SUBSCRIPTION = gql`
  subscription {
    updateCandidat {
      id
      uuid
      status
    }
  }
`;
