import React, { Component } from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import FormatAlignRight from "@material-ui/icons/FormatAlignRight";
import { withStyles } from "@material-ui/core";

const styles = theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: "#33A87D"
  },
  title: {
    flexGrow: 1
  },
  logo: {
    height: 50
  }
});

class Header extends Component {
  render() {
    const { classes } = this.props;
    return (
      <AppBar className={classes.root} position="static">
        <Toolbar>
          <Typography variant="h6" className={classes.title}>
            <img
              alt="WTTJ"
              className={classes.logo}
              src="https://cdn.welcometothejungle.co/wttj-front/assets/images/logos/wttj-square.svg"
            />
          </Typography>
          <FormatAlignRight />
        </Toolbar>
      </AppBar>
    );
  }
}

export default withStyles(styles)(Header);
