import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";

import Card from "@material-ui/core/Card";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import Chip from "@material-ui/core/Chip";
import ThumbUp from "@material-ui/icons/ThumbUp";
import Comment from "@material-ui/icons/Comment";
import Email from "@material-ui/icons/Email";

const styles = () => ({
  primary: {
    fontSize: 14
  },
  secondary: {
    fontSize: 12,
    color: "gray"
  },
  card: {
    height: 130,
    margin: 5,
    border: "2px solid #EFEDEF"
  },
  details: {
    display: "flex",
    alignItems: "center"
  },
  cover: {
    margin: 15,
    width: 60,
    height: 60,
    borderRadius: 30
  },
  controls: {
    borderTop: "1px solid #EFEDEF",
    display: "flex",
    alignItems: "center",
    color: "#BCBABC",
    fontSize: 14,
    padding: 5,
    height: 30
  },
  icon: {
    fontSize: 20,
    marginLeft: 15,
    marginRight: 5
  },
  chip: {
    backgroundColor: "#59B596",
    borderRadius: 10,
    color: "#fff",
    fontWeight: "bolder",
    height: 25
  }
});

class BoardDetail extends Component {
  render() {
    const { classes, candidat } = this.props;
    return (
      <Card className={classes.card}>
        <div className={classes.details}>
          <CardMedia
            className={classes.cover}
            image={candidat.imageUrl}
            title={candidat.name}
          />
          <CardContent className={classes.content}>
            <Typography component="h5" variant="h5" className={classes.primary}>
              {candidat.name}
            </Typography>
            <Typography variant="subtitle1" className={classes.secondary}>
              {candidat.description}
            </Typography>
          </CardContent>
        </div>
        <div className={classes.controls}>
          <Chip label={candidat.rating} className={classes.chip} />
          <ThumbUp className={classes.icon} />
          {candidat.likes_cnt}
          <Comment className={classes.icon} />
          {candidat.comments_cnt}
          <Email className={classes.icon} />
          {candidat.emails_cnt}
        </div>
      </Card>
    );
  }
}

export default withStyles(styles)(BoardDetail);
