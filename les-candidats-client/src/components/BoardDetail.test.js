import React from "react";
import TestRenderer from "react-test-renderer";

import BoardDetail from "./BoardDetail";
import { Card } from "@material-ui/core";

it("renders correctly", () => {
  const candidat = {
    comments_cnt: 2,
    description: "UK Premier ministre",
    emails_cnt: 7,
    id: "1",
    imageUrl:
      "https://specials-images.forbesimg.com/imageserve/59f5d442a7ea436b47b47db4/416x416.jpg?background=000000&cropX1=498&cropX2=1916&cropY1=74&cropY2=1493",
    likes_cnt: 3,
    name: "Theresa May",
    rating: "4.0",
    status: "to_meet",
    __typename: "Candidat"
  };

  const tree = TestRenderer.create(<BoardDetail candidat={candidat} />);
  expect(tree).toMatchSnapshot();
});
