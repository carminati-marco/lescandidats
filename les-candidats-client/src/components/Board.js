import React from "react";
import _ from "lodash";
import update from "immutability-helper";
import { withStyles } from "@material-ui/core";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import MenuIcon from "@material-ui/icons/Menu";
import NotificationImportant from "@material-ui/icons/NotificationImportant";
import Chip from "@material-ui/core/Chip";
import { DragDropContext, DropTarget, DragSource } from "react-dnd";
import HTML5Backend from "react-dnd-html5-backend";
import { createClient } from "../utils/apollo";
import BoardDetail from "./BoardDetail";
import {
  GET_CANDIDATS,
  UPDATE_STATE_CANDIDAT,
  CANDIDATS_SUBSCRIPTION
} from "../utils/gql";

const channels = ["to_meet", "interview"];
const labelsMap = {
  to_meet: "To meet",
  interview: "Interview"
};

const styles = theme => ({
  boardBar: {
    backgroundColor: "#253357"
  },
  board: {
    display: "flex",
    height: "85vh",
    backgroundColor: "#EFEDEF"
  },
  column: {
    minWidth: 200,
    width: 350,
    margin: 10,
    height: "98%",
    borderRadius: 5,
    border: "1px solid #EFEDEF",
    backgroundColor: "white"
  },
  columnHead: {
    padding: 10,
    fontSize: "1.2em",
    backgroundColor: "white",
    fontWeight: "bolder",
    textTransform: "uppercase"
  },
  chip: {
    marginLeft: 10,
    fontWeight: "bolder",
    width: 40,
    height: 30
  },
  item: {
    padding: 10,
    margin: 10,
    fontSize: "0.8em",
    cursor: "pointer",
    backgroundColor: "white"
  },
  title: {
    flexGrow: 1,
    backgroundColor: "#364973",
    color: "#fff",
    textTransform: "uppercase",
    marginLeft: 24,
    marginRight: 24,
    paddingLeft: 24,
    height: 64,
    lineHeight: "64px",
    fontSize: 14
  }
});

const { client } = createClient();

const BoardBar = ({ classes }) => (
  <AppBar position="static" className={classes.boardBar}>
    <Toolbar>
      <MenuIcon />
      <Typography variant="h6" className={classes.title}>
        Stage - Account manager
      </Typography>
      <NotificationImportant />
    </Toolbar>
  </AppBar>
);

class Board extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      candidats: []
    };
  }

  componentDidMount() {
    client.query({ query: GET_CANDIDATS }).then(result => {
      this.setState({
        candidats: result.data.candidats
      });
    });
    this.subscribe(this.updateState);
  }

  subscribe = updateQueryFunction => {
    // call the "subscribe" method on Apollo Client
    this.subscriptionObserver = client
      .subscribe({
        query: CANDIDATS_SUBSCRIPTION
      })
      .subscribe({
        next(data) {
          updateQueryFunction(
            data.data.updateCandidat.id,
            data.data.updateCandidat.status
          );
        },
        error(err) {
          //todo: create warning.
          console.error("err", err);
        }
      });
  };

  updateState = (id, status) => {
    const { candidats } = this.state;
    const task = _.find(candidats, { id: id });
    task.status = status;
    const taskIndex = candidats.indexOf(task);
    const newTasks = update(candidats, {
      [taskIndex]: { $set: task }
    });
    this.setState({ candidats: newTasks });
  };

  update = (id, status) => {
    this.updateState(id, status);
    client.mutate({
      mutation: UPDATE_STATE_CANDIDAT,
      variables: { id: id, status: status }
    });
  };

  render() {
    const { candidats } = this.state;
    const { classes } = this.props;
    return (
      <div>
        <BoardBar classes={classes} />

        <div className={classes.board}>
          {channels.map(channel => (
            <BoardColumn status={channel} key={channel}>
              <div className={classes.column}>
                <div className={classes.columnHead}>
                  {labelsMap[channel]}

                  <Chip
                    className={classes.chip}
                    label={_.filter(candidats, { status: channel }).length}
                  />
                </div>
                <div>
                  {_.filter(candidats, { status: channel }).map(candidat => (
                    <BoardItem
                      id={candidat.id}
                      key={candidat.id}
                      onDrop={this.update}
                    >
                      <BoardDetail candidat={candidat} />
                    </BoardItem>
                  ))}
                </div>
              </div>
            </BoardColumn>
          ))}
        </div>
      </div>
    );
  }
}

export default DragDropContext(HTML5Backend)(withStyles(styles)(Board));

// Column
const boxTarget = {
  drop(props) {
    return { name: props.status };
  }
};

class BoardColumn extends React.Component {
  render() {
    return this.props.connectDropTarget(<div>{this.props.children}</div>);
  }
}

BoardColumn = DropTarget("kanbanItem", boxTarget, (connect, monitor) => ({
  connectDropTarget: connect.dropTarget(),
  isOver: monitor.isOver(),
  canDrop: monitor.canDrop()
}))(BoardColumn);

// Item
const boxSource = {
  beginDrag(props) {
    return {
      name: props.id
    };
  },

  endDrag(props, monitor) {
    const dropResult = monitor.getDropResult();
    if (dropResult) {
      props.onDrop(monitor.getItem().name, dropResult.name);
    }
  }
};

class BoardItem extends React.Component {
  render() {
    return this.props.connectDragSource(<div>{this.props.children}</div>);
  }
}

BoardItem = DragSource("kanbanItem", boxSource, (connect, monitor) => ({
  connectDragSource: connect.dragSource(),
  isDragging: monitor.isDragging()
}))(BoardItem);
