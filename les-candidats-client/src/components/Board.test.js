import React from "react";
import renderer from "react-test-renderer";
import TestRenderer from "react-test-renderer";

import Board from "./Board";
import { MockedProvider } from "react-apollo/test-utils";

import { GET_CANDIDATS } from "../utils/gql";
import BoardDetail from "./BoardDetail";

const mocks = [
  {
    request: {
      query: GET_CANDIDATS
    },
    result: {
      data: {
        candidats: [
          {
            status: "interview",
            rating: "5.0",
            name: "Marco Carminati",
            likes_cnt: 6,
            imageUrl:
              "https://images.fineartamerica.com/images/artworkimages/mediumlarge/1/portrait-of-roaring-little-lion-sergey-taran.jpg",
            id: "3",
            emails_cnt: 5,
            description: "Productor des poires",
            comments_cnt: 2,
            __typename: "Candidat"
          },
          {
            status: "interview",
            rating: "4.2",
            name: "Woody Allen",
            likes_cnt: 12,
            imageUrl:
              "https://pbs.twimg.com/profile_images/980924434348470274/qXqtYvx0.jpg",
            id: "2",
            emails_cnt: 5,
            description: "Productor des poires",
            comments_cnt: 20,
            __typename: "Candidat"
          },
          {
            status: "to_meet",
            rating: "4.0",
            name: "Theresa May",
            likes_cnt: 3,
            imageUrl:
              "https://specials-images.forbesimg.com/imageserve/59f5d442a7ea436b47b47db4/416x416.jpg?background=000000&cropX1=498&cropX2=1916&cropY1=74&cropY2=1493",
            id: "1",
            emails_cnt: 7,
            description: "UK Premier ministre",
            comments_cnt: 2,
            __typename: "Candidat"
          }
        ]
      }
    }
  }
];

it("renders correctly", () => {
  const tree = renderer.create(<Board />).toJSON();
  expect(tree).toMatchSnapshot();
});

it("renders without error with mocked graphql", () => {
  const tree = TestRenderer.create(
    <MockedProvider mocks={mocks}>
      <Board />
    </MockedProvider>
  );
  expect(tree).toMatchSnapshot();
});
