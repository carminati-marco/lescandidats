# test/support/graphql_helper.ex
defmodule LesCandidatas.GraphqlHelper do
    use Phoenix.ConnTest
    # We need to set the default endpoint for ConnTest
    @endpoint LesCandidatasWeb.Endpoint
  
    def graphql_query(conn, options) do
      conn
      |> post("/api/graphql", build_query(options[:query], options[:variables]))
      |> json_response(200)
    end
  
    defp build_query(query, variables) do
      %{
        "query" => query,
        "variables" => variables
      }
    end
  end