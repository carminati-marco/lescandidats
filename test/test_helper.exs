import LesCandidatsWeb.FakeS3, only: [check_fakes3: 0]

check_fakes3()

{:ok, _} = Application.ensure_all_started(:ex_machina)

ExUnit.start()

Ecto.Adapters.SQL.Sandbox.mode(LesCandidats.Repo, :manual)
