defmodule GoStopWeb.SchemaTest do
  use LesCandidatsWeb.ConnCase, async: true

  # ...

  describe "candidats" do
    setup do
      alias LesCandidats.Candidats

      [Candidats.create(%{
        name: "Theresa May",
        description: "UK Premier ministre",
      })]
    end

    test "get and update candidat" do
      query = """
      {
        candidats {
          id
          name
          description
          status
        }
      }
      """
      res =
        conn()
        |> post("/graphql", %{query: query})
        |> json_response(200)

      # IO.inspect res
      [candidat = List.first(res["data"]["candidats"])]
      assert candidat["name"] == "Theresa May"
      assert candidat["description"] == "UK Premier ministre"
      assert candidat["status"] == "to_meet"

      {candidat_id, ""} = Integer.parse(candidat["id"])

      # PUT MUTATION=

      # IO.inspect candidat_id
      mutation = """      
      mutation { updateCandidat(id: #{candidat_id}, input:{status:"interview"}) 
        {
          result {
            status
          }
        }
      }    
      """

      res =
        conn()
        |> post("/graphql", %{query: mutation})
        |> json_response(200)
      
      assert res["data"]["updateCandidat"]["result"]["status"] == "interview"
      
    end
  end
  
  # ...  
  
end