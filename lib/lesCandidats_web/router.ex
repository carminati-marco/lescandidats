defmodule LesCandidatsWeb.Router do
  use LesCandidatsWeb, :router

  pipeline :api do
    plug(:accepts, ["json"])
    plug(LesCandidatsWeb.Plugs.Context)
  end

  scope "/" do
    pipe_through(:api)

    get("/", LesCandidatsWeb.HealthController, :healthz)
    get("/healthz", LesCandidatsWeb.HealthController, :healthz)
    forward("/graphql", Absinthe.Plug, schema: LesCandidatsWeb.Schema)

    if Mix.env() == :dev do
      forward("/graphiql", Absinthe.Plug.GraphiQL, schema: LesCandidatsWeb.Schema, socket: LesCandidatsWeb.UserSocket)
    end
  end
end
