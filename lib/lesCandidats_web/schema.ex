defmodule LesCandidatsWeb.Schema do
  use Absinthe.Schema

  import Kronky.Payload
  
  alias LesCandidats.Candidats.Candidat

  import_types(Absinthe.Type.Custom)
  import_types(Kronky.ValidationMessageTypes)
  import_types(LesCandidatsWeb.Schema.OptionTypes)
  import_types(LesCandidatsWeb.Schema.CandidatsTypes)
  import_types(LesCandidatsWeb.Queries.CandidatsQueries)
  import_types(LesCandidatsWeb.Mutations.CandidatsMutations)
  import_types(Absinthe.Plug.Types)

  payload_object(:boolean_payload, :boolean)
  payload_object(:candidat_payload, :candidat)
  
  query do
    import_fields(:candidats_queries)
  end

  mutation do
    import_fields(:candidats_mutations)
  end

  subscription do

    field :update_candidat, :candidat do
      arg(non_null(:id))

      trigger(
        :update_candidat,
        topic: fn
          %Candidat{} = candidat -> ["update_candidat"]
          _ -> []
        end
      )

      config(fn _args, _info ->
        {:ok, topic: "update_candidat"}
      end)
    end
  end

  def middleware(middleware, _field, %Absinthe.Type.Object{identifier: :mutation}) do
    middleware ++ [&build_payload/2]
  end

  def middleware(middleware, _field, _object) do
    middleware
  end

  # def plugins do
  #   [Absinthe.Middleware.Dataloader | Absinthe.Plugin.defaults()]
  # end

  # def dataloader() do
  #   alias LesCandidats.Recipes

  #   Dataloader.new()
  #   |> Dataloader.add_source(Recipes, Recipes.data())
  # end

  # def context(ctx) do
  #   Map.put(ctx, :loader, dataloader())
  # end
end
