defmodule LesCandidatsWeb.Queries.CandidatsQueries do
  use Absinthe.Schema.Notation

  import Ecto.Query, warn: false

  alias LesCandidats.Repo
  alias LesCandidats.Candidats
  alias LesCandidats.Candidats.Candidat

  object :candidats_queries do
    @desc "get candidats list"
    field :candidats, list_of(:candidat) do
      arg(:offset, :integer, default_value: 0)
      arg(:keywords, :string, default_value: nil)

      resolve(fn args, _ ->
        candidats =
          Candidat
          |> Candidats.search(args[:keywords])
          |> order_by(desc: :inserted_at)
          |> Repo.paginate(args[:offset])
          |> Repo.all()

        {:ok, candidats}
      end)
    end

    @desc "Number of candidats"
    field :candidats_count, :integer do
      arg(:keywords, :string, default_value: nil)

      resolve(fn args, _ ->
        candidats_count =
          Candidat
          |> Candidats.search(args[:keywords])
          |> Repo.count()

        {:ok, candidats_count}
      end)
    end

    @desc "fetch a Candidat by id"
    field :candidat, :candidat do
      arg(:id, non_null(:id))

      resolve(fn args, _ ->
        candidat = Candidat |> Repo.get!(args[:id])
        {:ok, candidat}
      end)
    end

    @desc "candidat not stored with default value"
    field :candidat_with_default_value, :candidat do
      resolve(fn _, _ ->
        {:ok, Candidat.default_values()}
      end)
    end

    @desc "candidat options for a field"
    field :candidat_options, list_of(:option) do
      arg(:field, non_null(:string))

      resolve(fn args, _ ->
        field = String.to_existing_atom(args[:field])

        options =
          Enum.map(Candidat.options()[field], fn opt ->
            %{label: opt, value: opt}
          end)

        {:ok, options}
      end)
    end
  end
end
