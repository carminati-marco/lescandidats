defmodule LesCandidatsWeb.Mutations.CandidatsMutations do
  use Absinthe.Schema.Notation
  import Ecto.Query, warn: false
  import LesCandidatsWeb.Helpers.ValidationMessageHelpers

  alias LesCandidatsWeb.Schema.Middleware
  alias LesCandidats.Repo
  alias LesCandidats.Candidats
  alias LesCandidats.Candidats.Candidat

  input_object :candidat_input do
    field(:status, :string)
  end

  object :candidats_mutations do
    @desc "Update a Candidat and return Candidat"
    field :update_candidat, :candidat_payload do
      arg(:id, non_null(:id))
      arg(:input, :candidat_input)

      resolve(fn %{input: params} = args, %{context: context} ->
        candidat =
          Candidat
          |> Repo.get!(args[:id])

        with true,
          {:ok, candidat_updated} <- Candidats.update(candidat, params) do
          {:ok, candidat_updated}
        else
          {:error, %Ecto.Changeset{} = changeset} -> {:ok, changeset}
          {:error, msg} -> {:ok, generic_message(msg)}
        end
      end)
    end
    
  end
end
