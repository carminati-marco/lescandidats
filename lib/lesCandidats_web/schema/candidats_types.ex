defmodule LesCandidatsWeb.Schema.CandidatsTypes do
  use Absinthe.Schema.Notation
  import Absinthe.Resolution.Helpers

  alias LesCandidats.Candidats
  alias LesCandidats.Candidats.Candidat

  @desc "A Candidat with title and content"
  object :candidat do
    field(:id, :id)
    field(:name, :string)
    field(:description, :string)
    field(:image_url, :string)
    field(:status, :string)
    field(:rating, :string)
    field(:likes_cnt, :integer)
    field(:comments_cnt, :integer)
    field(:emails_cnt, :integer)
    field(:uuid, :string)

  end

end
