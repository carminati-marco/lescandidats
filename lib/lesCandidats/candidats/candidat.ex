defmodule LesCandidats.Candidats.Candidat do
  use Ecto.Schema
  use Arc.Ecto.Schema
  import Ecto.Changeset
  alias LesCandidats.Candidats.Candidat

  @options %{
    status: ["to_meet", "interview"],
  }
  @default_values %{
    status: "to_meet",
    rating: 0.0,
    likes_cnt: 0,
    comments_cnt: 0,
    emails_cnt: 0,
  }

  schema "candidats" do
    field(:name, :string)
    field(:description, :string)
    field(:image_url, :string)
    field(:status, :string)
    field(:rating, :float)
    field(:likes_cnt, :integer)
    field(:comments_cnt, :integer)
    field(:emails_cnt, :integer)
    field(:uuid, :string)
    timestamps(type: :utc_datetime)
  end

  @dialyzer {:no_match, changeset: 2}
  def changeset(%Candidat{} = candidat, attrs) do
    attributes = attrs
    
    candidat
    |> cast(attributes, [:name, :description, :image_url, :status, :rating, :likes_cnt, :comments_cnt, :emails_cnt, :uuid ])
    |> check_uuid
    |> validate_required([:name, :description, :uuid])
  end

  def options(), do: @options
  def default_values(), do: @default_values

  defp check_uuid(changeset) do
    if get_field(changeset, :uuid) == nil do
      force_change(changeset, :uuid, Ecto.UUID.generate())
    else
      changeset
    end
  end
end
