defmodule LesCandidats.Candidats do
  import Ecto.Query, warn: false
  import Ecto.Changeset, only: [put_assoc: 3]
  alias LesCandidats.Repo
  alias LesCandidats.Candidats.Candidat
  
  def search(query, nil), do: query
  
  def create(attrs) do
    %Candidat{}
    |> Candidat.changeset(attrs)
    |> Repo.insert()
  end

  def update(%Candidat{} = candidat, attrs) do
    candidat
    |> Candidat.changeset(attrs)
    |> Repo.update()
  end

  def delete(%Candidat{} = candidat) do
    {:ok, candidat} = candidat |> Repo.delete()
    {:ok, candidat}
  end

  def data() do
    Dataloader.Ecto.new(Repo, query: &query/2)
  end

  def query(queryable, _) do
    queryable
  end
end
