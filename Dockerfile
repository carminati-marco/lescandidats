FROM elixir:1.8
RUN apt-get update && apt-get install --yes postgresql-client
ADD . /app
WORKDIR /app
RUN mix local.hex --force
RUN mix archive.install --force https://github.com/phoenixframework/archives/raw/master/phx_new.ez
EXPOSE 4000
CMD ["sh", "run.sh"]