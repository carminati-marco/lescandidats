clean:
	docker-compose kill && docker-compose rm --force

bash_backend:
	docker-compose exec web bash

build: clean
	docker-compose build
	docker-compose up -d

exec_web:
	cd les-candidats-client/ && yarn && yarn start


build_all: build exec_web