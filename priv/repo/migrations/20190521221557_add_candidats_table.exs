defmodule LesCandidats.Repo.Migrations.AddCandidatsTable do
  use Ecto.Migration

  def change do
    create table(:candidats) do
      add :name, :string, null: false
      add :description, :string, null: false
      add :image_url, :string, null: true
      add :status, :string, default: "to_meet", null: false
      add :rating, :float, default: 0.0, null: false
      add :likes_cnt, :integer, default: 0, null: false
      add :comments_cnt, :integer, default: 0, null: false
      add :emails_cnt, :integer, default: 0, null: false
      add :uuid, :string
      add :inserted_at, :timestamptz
      add :updated_at, :timestamptz
    end
  end
end
