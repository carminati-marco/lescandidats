# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     LesCandidats.Repo.insert!(%LesCandidats.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
# credo:disable-for-this-file

alias LesCandidats.Repo
alias LesCandidats.Candidats
alias LesCandidats.Candidats.Candidat


Candidat |> Repo.delete_all()

Candidats.create(%{
  name: "Theresa May",
  description: "UK Premier ministre",
  image_url: "https://specials-images.forbesimg.com/imageserve/59f5d442a7ea436b47b47db4/416x416.jpg?background=000000&cropX1=498&cropX2=1916&cropY1=74&cropY2=1493",
  rating: 4,
  likes_cnt: 3, 
  comments_cnt: 2, 
  emails_cnt: 7,
})

Candidats.create(%{
  name: "Woody Allen",
  description: "Productor des poires",
  image_url: "https://pbs.twimg.com/profile_images/980924434348470274/qXqtYvx0.jpg",
  status: "interview",
  rating: 4.2,
  likes_cnt: 12, 
  comments_cnt: 20, 
  emails_cnt: 5,
})

Candidats.create(%{
    name: "Marco Carminati",
    description: "Productor des poires",
    image_url: "https://images.fineartamerica.com/images/artworkimages/mediumlarge/1/portrait-of-roaring-little-lion-sergey-taran.jpg",
    status: "interview",
    rating: 5,
    likes_cnt: 6, 
    comments_cnt: 2, 
    emails_cnt: 5,
})